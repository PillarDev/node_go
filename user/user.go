package user

//
// Created by pillardev on 17.11.2019.
//

type user struct {
	name string
	surName string
	eMail string
	phone uint64
	societyUrl string
	gender bool
	birthday uint32
	metaData string
}

var userStorage [] user

func createUser(newName string,
				newSurname string,
				newMail string,
				newPhone uint64,
				newLink string,
				newGender bool,
				newBirthday uint32) {
					
					var newUser user = user{name:newName,
											surName:newSurname,
											eMail:newMail,
											phone:newPhone,
											societyUrl:newLink,
											gender:newGender,
											birthday:newBirthday}
		userStorage[len(userStorage)] = newUser
}

func getUser(id int) user {
	return userStorage[id]
}

func setUserName(id int, name string) {
	userStorage[id].name = name
}

func setUserSurName(id int, surName string) {
	userStorage[id].surName = surName
}

func setUserMail(id int, mail string) {
	userStorage[id].eMail = mail
}

func setUserPhone(id int, phone uint64) {
	userStorage[id].phone = phone
}

func setUserLink(id int, link string) {
	userStorage[id].societyUrl = link
}
