package service

var cityCounter map[uint16] uint16

func makeServiceID(countryId uint8, cityID uint16) uint64 {
	
	var serviceId = uint64(countryId) + uint64(cityID) + uint64(cityCounter[cityID]) // todo : will make number concat
	return serviceId
}