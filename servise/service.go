package service

//
// Created by pillardev on 17.11.2019.
//

type service struct {
	countryCode uint8
	cityCode uint16
	address string
	eMail string
	phone uint64
	zipCode uint32
	publicUrl string
	metaData string
}

var serviceStorage map[uint64] service

func createService( newCountryCode uint8,
					newCityCode uint16,
					newAddress string,
					newEmail string,
					newPhone uint64,
					newZipCode uint32,
					newPublicUrl string) {
	
	var newService = service{countryCode:newCountryCode,
							cityCode:newCityCode,
							address:newAddress,
							eMail:newEmail,
							phone:newPhone,
							zipCode:newZipCode,
							publicUrl:newPublicUrl}
	
	serviceStorage[makeServiceID(newCountryCode, newCityCode)] = newService
					}

func getService(id uint64) service {
	return serviceStorage[id]
}

func setServiceAddress(id uint64, newAddress string) {
	tempUser := serviceStorage[id]
	tempUser.address = newAddress
	serviceStorage[id] = tempUser
}

func setEmail(id uint64, newEmail string) {
	tempUser := serviceStorage[id]
	tempUser.eMail = newEmail
	serviceStorage[id] = tempUser
}

func setPhone(id uint64, newPhone uint64) {
	tempUser := serviceStorage[id]
	tempUser.phone = newPhone
	serviceStorage[id] = tempUser
}


func setPublicUrl(id uint64, link string) {
	tempUser := serviceStorage[id]
	tempUser.publicUrl = link
	serviceStorage[id] = tempUser
}

func setMetaData(id uint64, newMetaData string) {
	tempUser := serviceStorage[id]
	tempUser.metaData = newMetaData
	serviceStorage[id] = tempUser
}